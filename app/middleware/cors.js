import config from 'config'

function checkOriginAgainstWhitelist(ctx) {
    const requestOrigin = ctx.accept.headers.origin;
    if (config.server.whitelisted_origins.includes(requestOrigin) ) {
        return requestOrigin
    } else if (config.server.whitelisted_origins.includes('*')){
        return '*'
    }
    return ctx.throw(`🙈 ${requestOrigin} is not a valid origin`)
}

export default (ctx) => {
    checkOriginAgainstWhitelist(ctx)
}