import Router from 'koa-router';
import example from '../controllers/example';
import convert from 'koa-convert';
import KoaBody from 'koa-body';

const router = new Router(),
      koaBody = convert(KoaBody());

    router
        .get('/', koaBody, async(ctx) =>{
            ctx.status = 200
            ctx.body = example.run()
        })

export function routes () { return router.routes() }
export function allowedMethods () { return router.allowedMethods() }