import config from 'config'
const run = () => {
    return `Server is Up, hello from backend on port ${config.server.port}`
}
export default {
    run: run
}